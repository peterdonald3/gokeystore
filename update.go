package gokeystore

/*
Description:

	updates keyStore with updated values.
	If autocreate enabled then adds request to keyStore.
	Returns notification explaining if value does not exist.
*/
func (rKeys *KeyReq) UpdateKeys() *KeyReq {
	rKeys.update = true

	return rKeys
}
