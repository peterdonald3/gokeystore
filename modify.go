package gokeystore

import (
	"fmt"
	"sync"
)

func modify(kReq *KeyReq) *KeyResp {

	var (
		kResp *KeyResp
		wg    sync.WaitGroup
	)

	// Setup Response

	kResp = &KeyResp{
		Keys:          kReq.Keys,
		Notifications: kReq.Notifications,
		AutoCreate:    kReq.AutoCreate,
	}

	wg.Add(1)
	q := make(chan *KeyResp, 1)

	go func(kResp *KeyResp) {
		defer wg.Done()
		kResp = modifyKeyStore(kReq, kResp)
		q <- kResp
	}(kResp)

	wg.Wait()
	close(q)
	kResp = <-q
	return kResp
}

func modifyKeyStore(kReq *KeyReq, kResp *KeyResp) *KeyResp {

	if !kReq.create && !kReq.read && !kReq.update && !kReq.delete {
		return kResp
	}
	if kReq.create {
		keyStore.m.Lock()
		defer keyStore.m.Unlock()
		if len(*kReq.Keys) == 0 {
			kResp.addNotification(noKeysErr)
		}
		for _, v := range *kReq.Keys {
			_, f := keyStore.k[v.Key]
			if f {
				kResp.addNotification(fmt.Sprintf("value for [%v] already exists", v.Key))
			}

			if !f {
				kResp.addNotification(fmt.Sprintf("value for [%v] created", v.Key))
				keyStore.k[v.Key] = keyData{
					Key:   v.Key,
					Value: v.Value,
				}
			}
		}
		return kResp
	}
	if kReq.read {
		if len(*kReq.Keys) == 0 {
			kResp.addNotification(noKeysErr)
			return kResp // can't read if you don't provide information
		}
		keyStore.m.RLock()
		defer keyStore.m.RUnlock()
		if kReq.Keys == nil {
			if len(keyStore.k) == 0 {
				kResp.addNotification("there are no keys stored at the moment")
				// Return early as no values to read
				return kResp
			}

			kReq.Keys = &[]keyData{}
			for _, v := range keyStore.k {
				*kReq.Keys = append(*kReq.Keys, v)
			}
		}

		for i, v := range *kReq.Keys {
			k, f := keyStore.k[v.Key]
			if f {
				(*kReq.Keys)[i] = keyData{
					Key:   k.Key,
					Value: k.Value,
				}
			}
			if !f {
				kResp.addNotification(fmt.Sprintf("key value [%v] does not exist", v.Key))
			}
		}
		return kResp
	}
	if kReq.update {
		keyStore.m.Lock()
		defer keyStore.m.Unlock()
		for _, v := range *kReq.Keys {
			_, f := keyStore.k[v.Key]

			if f {
				keyStore.k[v.Key] = keyData{
					Key:   v.Key,
					Value: v.Value,
				}
			}

			if !f && kReq.AutoCreate {
				kResp.addNotification(fmt.Sprintf("value for [%v] could not be found and was created", v.Key))
				keyStore.k[v.Key] = keyData{
					Key:   v.Key,
					Value: v.Value,
				}
			}

			if !f && !kReq.AutoCreate {
				kResp.addNotification(fmt.Sprintf("value for [%v] could not be found and autocreate is not enable, please create key", v.Key))
			}
		}
		return kResp
	}
	if kReq.delete {
		keyStore.m.Lock()
		defer keyStore.m.Unlock()
		for _, v := range *kReq.Keys {
			_, f := keyStore.k[v.Key]

			if f {
				delete(keyStore.k, v.Key)
				kResp.addNotification(fmt.Sprintf("value for [%v] was successfully deleted", v.Key))
			}

			if !f {
				kResp.addNotification(fmt.Sprintf("value for [%v] could not be found", v.Key))
			}
		}
		return kResp
	}
	return kResp
}
