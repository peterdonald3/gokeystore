package gokeystore

import (
	"reflect"
	"testing"
)

func TestKeyReq_addNotification(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		in   *KeyResp
		args args
		out  *KeyResp
	}{
		{
			"Test 1",
			&KeyResp{},
			args{"test"},
			&KeyResp{
				Notifications: &[]string{"test"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.in.addNotification(tt.args.s); !reflect.DeepEqual(got, tt.out) {
				t.Errorf("KeyReq.addNotification() = %v, want %v", got, tt.out)
			}
		})
	}
}
