package gokeystore

import (
	"fmt"
	"testing"
)

func keyDataTestHandler(index *int, in *keyData, out *keyData, testName string, functionName string, t *testing.T) bool {
	const (
		structName = "keyData"
	)
	var (
		errOccured = false
	)
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("\n\n*********************************************\n\nRecovered in %v %v %v - %v\n\n*********************************************\n\n",
				testName, functionName, structName, r)
			t.Error("FATAL PANIC OCCURRED CHECK CONSOLE FOR DETAILS")
		}
	}()
	if in.Key != out.Key {
		errOccured = true
		t.Errorf("\n%#v | %#v ERROR: %#v.[%#v] does not match:\n IN: \n%#v\n, OUT: \n%#v\n",
			testName, functionName, structName, "Key", in.Key, out.Key)
	}
	if in.Value != out.Value {
		errOccured = true
		t.Errorf("\n%#v | %#v ERROR: %#v.[%#v] does not match:\n IN: \n%#v\n, OUT: \n%#v\n",
			testName, functionName, structName, "Value", in.Value, out.Value)
	}
	return errOccured
}

func sliceKeyDataTestHandler(in *[]keyData, out *[]keyData, testName string, functionName string, t *testing.T) {
	const (
		structName = "[]keyData"
	)
	var (
		allfound          = []bool{}
		firstProcKeyData  = []int{}
		secondProcKeyData = []int{}
	)
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("\n\n*********************************************\n\nRecovered in %v %v %v - %v\n\n*********************************************\n\n",
				testName, functionName, structName, r)
			t.Error("FATAL PANIC OCCURRED CHECK CONSOLE FOR DETAILS")
		}
	}()

	// something missing or unexpected
	if len(*in) != len(*out) {
		allfound = append(allfound, false)
		t.Errorf("\n%#v | %#v ERROR: length of %#v does not match:\n IN: \n%#v\n, OUT: \n%#v\n",
			testName, functionName, structName, len(*in), len(*out))
	}

	for i, first := range *in {
		// add to processed keys
		firstProcKeyData = append(firstProcKeyData, i)
		found := false
		for ii, second := range *out {
			if first.Key == second.Key && first.Value == second.Value {
				// check if already processed/handled
				if ContainsInt(secondProcKeyData, ii) {
					// might have to consider if duplicates can be submitted...
					break
				}
				// add to processed keys
				secondProcKeyData = append(secondProcKeyData, ii)

				found = true
				break
			}
		}
		allfound = append(allfound, found)
	}
	if !SameBool(allfound) && ContainsFalse(allfound) {
		t.Errorf("\n%#v | %#v ERROR: something didn't match -  %#v does not match:\n IN: \n%#v\n, OUT: \n%#v\n",
			testName, functionName, structName, len(*in), len(*out))
	}
}

func keyRespTestHandler(index *int, in *KeyResp, out *KeyResp, testName string, functionName string, structName string, t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("\n\n*********************************************\n\nRecovered in %v %v %v - %v\n\n*********************************************\n\n",
				testName, functionName, structName, r)
			t.Error("FATAL PANIC OCCURRED CHECK CONSOLE FOR DETAILS")
		}
	}()

	sliceKeyDataTestHandler(in.Keys, out.Keys, testName, "keyRespTestHandler", t)

	if in.Notifications == nil && out.Notifications == nil {

	} else if *in.Notifications != nil && *out.Notifications != nil {
		if !StringArrayMatch(*in.Notifications, *out.Notifications) {
			t.Errorf("\n%#v | %#v, %#v ERROR: notifications does not match:\n IN: \n%#v\n, OUT: \n%#v\n",
				testName, functionName, structName, len(*in.Notifications), len(*out.Notifications))
			for i, v := range *in.Notifications {
				t.Errorf("\n%#v | IN: \n%#v\n",
					i, v)
			}
			for i, v := range *out.Notifications {
				t.Errorf("\n%#v | OUT: \n%#v\n",
					i, v)
			}
		}
	} else {
		t.Errorf("\n%#v | %#v, %#v ERROR: notifications does not match:\n IN: \n%#v\n, OUT: \n%#v\n",
			testName, functionName, structName, len(*in.Notifications), len(*out.Notifications))
		for i, v := range *in.Notifications {
			t.Errorf("\n%#v | IN: \n%#v\n",
				i, v)
		}
		for i, v := range *out.Notifications {
			t.Errorf("\n%#v | OUT: \n%#v\n",
				i, v)
		}
	}

}

// Returns true if both string slices match (disregards order)
func StringArrayMatch(got []string, exp []string) bool {
	allfound := []bool{}
	for _, first := range got {
		found := false
		for _, second := range exp {
			if first == second {
				found = true
				break
			}
		}
		allfound = append(allfound, found)
	}
	if len(got) != len(exp) { // something missing or unexpected
		allfound = append(allfound, false)
	}
	if SameBool(allfound) && !ContainsFalse(allfound) {
		return true
	}
	return false
}

// Returns true if both string slices match (disregards order)
func IntArrayMatch(got []int, exp []int) bool {
	allfound := []bool{}
	for _, first := range got {
		found := false
		for _, second := range exp {
			if first == second {
				found = true
				break
			}
		}
		allfound = append(allfound, found)
	}
	if len(got) != len(exp) { // something missing or unexpected
		allfound = append(allfound, false)
	}
	if SameBool(allfound) && !ContainsFalse(allfound) {
		return true
	}
	return false
}

// returns true if all values in a []bool match
func SameBool(s []bool) bool {
	for i := 0; i < len(s); i++ {
		if s[i] != s[0] {
			return false
		}
	}
	return true
}

// checks if string slice contains string, returns true if found
func ContainsFalse(b []bool) bool {
	for _, v := range b {
		if !v {
			return true
		}
	}
	return false
}

// checks if string slice contains string, returns true if found
func ContainsInt(s []int, i int) bool {
	for _, v := range s {
		if v == i {
			return true
		}
	}
	return false
}
