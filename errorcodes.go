package gokeystore

//type noKeyErr string = "no keys provided"

const (
	// Internal Codes
	noKeysErr = "no keys provided" // 400111111

	// defaults
	errCodeInvalid = "there is an internal server error: code does not provide a valid error code" // 500111111
)

/*
	Returns errorcode for debugging

	Mostly likely http code plus 6 digits

	Defaults to 500111111 which means that an internal service is not providing the correct error code...
*/
func StatusCode(errCode string) int32 {
	switch errCode {
	// Bad Request Codes [400]
	case noKeysErr:
		return 400111111

	// Internal Codes [500]
	default:
		return 500111111
	}
}
