package gokeystore

import "sync"

/*
Description:

	basic key/value struct.
*/
type keyData struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

/*
Description:

	struct for handling JSON requests.
	if AutoCreate is true, any PUT requests will automatically create the key sent for update if it does not exist.
*/
type KeyReq struct {
	Keys          *[]keyData `json:"keys"`
	Notifications *[]string  `json:"notifications,omitempty"`
	AutoCreate    bool       `json:"autocreate,omitempty"`
	create        bool
	read          bool
	update        bool
	delete        bool
}

type KeyResp struct {
	Keys          *[]keyData `json:"keys"`
	Notifications *[]string  `json:"notifications,omitempty"`
	AutoCreate    bool       `json:"autocreate,omitempty"`
}

/*
Description:

	Global data store for keys.
*/

type kStore struct {
	k map[string]keyData
	m sync.RWMutex
}

var keyStore kStore

func init() {
	keyStore = kStore{
		k: make(map[string]keyData),
		m: sync.RWMutex{},
	}
}
