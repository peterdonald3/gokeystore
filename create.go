package gokeystore

/*
Description:

	creates key in store if does not exist already.
	Returns notification explaining if value exists or already existed.
*/
func (rKeys *KeyReq) CreateKeys() *KeyReq {
	rKeys.create = true

	return rKeys
}
