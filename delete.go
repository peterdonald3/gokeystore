package gokeystore

/*
Description:

	Deletes key from store if exists.
	Returns notification if found or not found.
*/
func (rKeys *KeyReq) DeleteKeys() *KeyReq {
	rKeys.delete = true

	return rKeys
}
