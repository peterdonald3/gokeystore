package gokeystore

/*
Description:

	Reads key from store for those requested.
	Returns all keys if none specific request.
	Returns notification if no keys are in store at time of request.
*/
func (rKeys *KeyReq) ReadKeys() *KeyReq {
	rKeys.read = true

	return rKeys
}
