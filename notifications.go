package gokeystore

/*
Description:

	Used to add a notification to KeyReq.Notification.
	Initialises notifications if does not exist.
*/
func (k *KeyResp) addNotification(s string) *KeyResp {
	if k.Notifications == nil {
		k.Notifications = &[]string{}
	}
	*k.Notifications = append(*k.Notifications, s)
	return k
}
