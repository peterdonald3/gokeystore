package gokeystore

import (
	"reflect"
	"testing"
)

func Test_modify(t *testing.T) {
	type args struct {
		kReq *KeyReq
	}
	tests := []struct {
		name string
		args args
		want *KeyResp
	}{
		// Sequential tests
		{
			"Test 1 - no keys provided",
			args{
				&KeyReq{
					Keys:          &[]keyData{},
					Notifications: nil,
					AutoCreate:    true,
					create:        true,
				},
			},
			&KeyResp{
				Keys:          &[]keyData{},
				Notifications: &[]string{noKeysErr},
				AutoCreate:    true,
			},
		},
		{
			"Test 2 - add two keys",
			args{
				&KeyReq{
					Keys: &[]keyData{
						{
							Key:   "test 2 - key 1",
							Value: "test 2 - value 1",
						},
						{
							Key:   "test 2 - key 2",
							Value: "test 2 - value 2",
						},
					},
					Notifications: nil,
					AutoCreate:    true,
					create:        true,
				},
			},
			&KeyResp{
				Keys: &[]keyData{
					{
						Key:   "test 2 - key 1",
						Value: "test 2 - value 1",
					},
					{
						Key:   "test 2 - key 2",
						Value: "test 2 - value 2",
					},
				},
				Notifications: &[]string{
					"value for [test 2 - key 1] created",
					"value for [test 2 - key 2] created",
				},
				AutoCreate: true,
			},
		},
		{
			"Test 3 - read keys provided",
			args{
				&KeyReq{
					Keys: &[]keyData{
						{
							Key: "test 2 - key 1",
						},
						{
							Key: "test 2 - key 2",
						},
					},
					read: true,
				},
			},
			&KeyResp{
				Keys: &[]keyData{
					{
						Key:   "test 2 - key 1",
						Value: "test 2 - value 1",
					},
					{
						Key:   "test 2 - key 2",
						Value: "test 2 - value 2",
					},
				},
				Notifications: nil,
				AutoCreate:    true,
			},
		},
		{
			"Test 4 - read keys (nothing provided)",
			args{
				&KeyReq{
					Keys: &[]keyData{},
					read: true,
				},
			},
			&KeyResp{
				Keys:          &[]keyData{},
				Notifications: &[]string{noKeysErr},
				AutoCreate:    true,
			},
		},
	}
	for i, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := modify(tt.args.kReq); !reflect.DeepEqual(got, tt.want) {
				keyRespTestHandler(&i, got, tt.want, tt.name, "modify", "Resp", t)
			}
		})
	}
}
